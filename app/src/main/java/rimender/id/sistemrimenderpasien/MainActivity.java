package rimender.id.sistemrimenderpasien;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goLoginPasien(View view){
        Intent intent = new Intent(this, LoginActivityPasien.class);
        startActivity(intent);
    }

    public void  goLoginDokter(View view){
        Intent intent = new Intent(this, LoginActivityDokter.class);
        startActivity(intent);
    }

    public void  goLoginPMO(View view){
        Intent intent = new Intent(this, LoginActivityPmo.class);
        startActivity(intent);
    }

//    public void goHomePasien(View view){
//
//    }
}
